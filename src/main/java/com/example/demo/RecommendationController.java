package com.example.demo;

import Util.DatabaseConnection;
import Util.HSBCProvidedUtil;
import modules.Holding;
import modules.Portfolio;
import modules.Recommendation;
import modules.Transaction;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.HttpClientErrorException;

import java.sql.*;
import java.util.HashMap;

import static java.lang.Math.abs;

public class RecommendationController {

    public static ResponseEntity<Recommendation> getRebalanceRecommendation(@PathVariable String id, @RequestHeader HttpHeaders headers){

        Recommendation recommendation = new Recommendation();
        recommendation.setRecommendationId(id);     //recommendation_id is currently set to Portfolio_id
        //get the asset mix preference
        Portfolio portfolio = PreferenceController.getInitialPreference(id).getBody();
        HashMap<String, Integer> allocations = portfolio.getAllocations();

        String customerId = HSBCProvidedUtil.validateHeader(headers);

        if(customerId == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try{
            HashMap<String, Holding> holdings = HSBCProvidedUtil.getHoldings(headers, id);
            //compute the percentage of each holding and compare with preference percentage
            int totalBalance = 0;
            for (String fundId : holdings.keySet()) {
                totalBalance += holdings.get(fundId).getBalance();
            }
            int deviation = Integer.parseInt(portfolio.getDeviation());
            boolean inRange = true;
            for (String fundId : allocations.keySet()) {
                //compute a percentage of each fund holding
                float f_balance = holdings.get(fundId).getBalance();
                float f_percentage = f_balance * 100.0f / totalBalance;
                // if any percentage difference is outside of the range of the deviation that user set, a recommendation should be provided
                float percentage_diff = (float) allocations.get(fundId) - f_percentage;
                if (abs(percentage_diff) > (float) deviation) {
                    inRange = false;
                }
            }

            // if all percentage differences are within the range of the deviation, no recommendation is needed
            if (inRange) {
                return new ResponseEntity<>(recommendation, HttpStatus.OK);
            }

            //build the recommendation
            for (String fundId : holdings.keySet()) {
                Holding h = holdings.get(fundId);
                Transaction transaction = new Transaction();
                transaction.setFundId(fundId);

                //compute the transaction units
                float unitPrice = h.getBalance() / h.getUnits();
                int units = Math.round((totalBalance * (allocations.get(fundId) / 100.f) - h.getBalance()) / unitPrice);

                if (units == 0)
                    continue;
                else if (units > 0)
                    transaction.setAction("buy");
                else
                    transaction.setAction("sell");

                transaction.setUnits(abs(units));
                recommendation.addTransaction(transaction);
            }


            //insert the recommendation into database
            Connection conn = DatabaseConnection.setUpConnection();

            // mysql insert statement for recommendation table
            String query = " insert into recommendations (portfolio_id, recommendation_id)"
                    + " values (?, ?)  ON DUPLICATE KEY UPDATE recommendation_id=?";

            // create preparedstatement
            PreparedStatement preparedStmt_rec = conn.prepareStatement(query);
            preparedStmt_rec.setInt(1, Integer.parseInt(portfolio.getId()));
            preparedStmt_rec.setInt(2, Integer.parseInt(recommendation.getRecommendationId()));
            preparedStmt_rec.setInt(3, Integer.parseInt(recommendation.getRecommendationId()));
            preparedStmt_rec.execute();

            for (int i = 0; i < recommendation.getTransactions().size(); i++) {
                Transaction t = recommendation.getTransactions().get(i);

                // mysql insert statement for transaction table
                String query0 = " insert into transactions (recommendation_id, fund_id, action, units)"
                        + " values (?, ?, ?, ?) ON DUPLICATE KEY UPDATE fund_id=?, action=?, units=?";

                // create preparedstatement
                PreparedStatement preparedStmt_trans = conn.prepareStatement(query0);
                preparedStmt_trans.setInt(1, Integer.parseInt(recommendation.getRecommendationId()));
                preparedStmt_trans.setInt(2, Integer.parseInt(t.getFundId()));
                preparedStmt_trans.setString(3, t.getAction());
                preparedStmt_trans.setInt(4, t.getUnits());
                preparedStmt_trans.setInt(5, Integer.parseInt(t.getFundId()));
                preparedStmt_trans.setString(6, t.getAction());
                preparedStmt_trans.setInt(7, t.getUnits());
                preparedStmt_trans.execute();
            }

            conn.close();
        } catch (HttpClientErrorException httpClientErrorException) {
            return new ResponseEntity<>(recommendation, httpClientErrorException.getStatusCode());
        } catch (SQLException e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Got an exception!");
            System.err.println(e.getMessage());
        }

        return new ResponseEntity<>(recommendation, HttpStatus.OK);
        }
}
