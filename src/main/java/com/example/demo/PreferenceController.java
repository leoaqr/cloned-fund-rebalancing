package com.example.demo;

import Util.DatabaseConnection;
import modules.Portfolio;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

public class PreferenceController {

    public static ResponseEntity<Portfolio> getInitialPreference(@PathVariable String id) {
        Portfolio portfolio = new Portfolio();
        // TODO: verify if portfolio id is a valid portfolio of the customer id past from header
        // TODO: If the portfolio id is not valid, return HttpStatus.BAD_REQUEST
        portfolio.setId(id);
        Connection conn = DatabaseConnection.setUpConnection();
        if (conn == null) {
            return new ResponseEntity<>(portfolio, HttpStatus.BAD_REQUEST);
        }
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * from portfolio WHERE  id = " + id);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                String deviation = rs.getString(2);
                portfolio.setDeviation(deviation);
                String type = rs.getString(3);
                portfolio.setType(type);
            }

            PreparedStatement allocationsStatement = conn.prepareStatement("select * from allocations where portfolio_id =" + id);
            ResultSet data = allocationsStatement.executeQuery();

            HashMap<String, Integer> allocations = new HashMap<>();

            while (data.next()) {
                String fundId = data.getString(1);
                Integer percentage = Integer.parseInt(data.getString(3));
                allocations.put(fundId, percentage);
            }
            portfolio.setAllocations(allocations);
            conn.close();

        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }

        return new ResponseEntity<>(portfolio, HttpStatus.OK);
    }

}
