package com.example.demo;

import Util.DatabaseConnection;
import modules.Portfolio;
import modules.Recommendation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/index")
	String home() {
		return "Welcome to Rebalance Fund Service System - Team REST!";
	}

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @RequestMapping(value = "/portfolio/{id}", method = POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<Portfolio> createInitialPreference(@PathVariable String id, @RequestBody Portfolio portfolio) {
        ArrayList<String> fundIds = new ArrayList<>();
        ArrayList<Integer> precentages = new ArrayList<>();
        if (portfolio != null) {
            portfolio.setId(id);
            // TODO: check fundIds are valid ids
            // TODO: check allocations sum up to 100
            // TODO: check devication is [0-5]
            portfolio.getAllocations().keySet().forEach(fundId -> {
                fundIds.add(fundId);
                precentages.add(portfolio.getAllocations().get(fundId).intValue());
            });
        }
        try {
            Connection conn = DatabaseConnection.setUpConnection();
            if (conn == null) {
                return new ResponseEntity<>(portfolio, HttpStatus.BAD_REQUEST);
            }
            // mysql insert statement for portfolio table
            String query = " insert into portfolio (id, deviation, type)"
                    + " values (?, ?, ?)";

            // create the mysql insert preparedstatement for portfolio
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt(1, Integer.parseInt(portfolio.getId()));
            preparedStmt.setInt(2, Integer.parseInt(portfolio.getDeviation()));
            preparedStmt.setString(3, portfolio.getType());
            preparedStmt.execute();

            // create the mysql insert preparedstatement for allocations
            for (String fundId : fundIds) {
                String allocationsInsertQuery = " insert into allocations (fund_id, portfolio_id, percentage)"
                        + " values (?, ?, ?)";
                PreparedStatement preparedStmtAllocation = conn.prepareStatement(allocationsInsertQuery);
                preparedStmtAllocation.setInt(1, Integer.parseInt(fundId));
                preparedStmtAllocation.setInt(2, Integer.parseInt(portfolio.getId()));
                preparedStmtAllocation.setInt(3, portfolio.getAllocations().get(fundId));
                preparedStmtAllocation.execute();
            }

            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
        return new ResponseEntity<>(portfolio, HttpStatus.OK);
    }

    @RequestMapping(value = "/portfolio/{id}/deviation", method = PUT, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<Map<String, Integer>> updateDeviation(@PathVariable String id, @RequestBody Map<String, Integer> request) {
        int deviation = request.getOrDefault("deviation", -1);
        if (!request.containsKey("deviation") || deviation < 0 || deviation > 5) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            // create a mysql database connection
            Connection conn = DatabaseConnection.setUpConnection();

            // mysql update statement for portfolio table
            String query = "update portfolio set deviation = ? where id = ?";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt(1, deviation);
            preparedStmt.setInt(2, Integer.parseInt(id));
            preparedStmt.executeUpdate();
            preparedStmt.close();
            conn.close();
        } catch (SQLException e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @RequestMapping(value = "/portfolio/{id}", method = GET)
    @ResponseBody
    public ResponseEntity<Portfolio> getInitialPreference(@PathVariable String id) {
        return PreferenceController.getInitialPreference(id);
    }


    @RequestMapping(value = "/portfolio/{id}/rebalance", method = POST)
    @ResponseBody
    public ResponseEntity<Recommendation> getRebalanceRecommendation(@PathVariable String id, @RequestHeader HttpHeaders headers) {
	    return RecommendationController.getRebalanceRecommendation(id,headers);
    }
}