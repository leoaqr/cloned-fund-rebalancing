package modules;

import java.util.ArrayList;

public class Recommendation {
    private String recommendationId;
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    public Recommendation(){}

    public void setRecommendationId(String recommendationId){
        this.recommendationId = recommendationId;
    }

    public String getRecommendationId(){
        return this.recommendationId;
    }

    public void addTransaction(Transaction transaction){
        this.transactions.add(transaction);
    }

    public ArrayList<Transaction> getTransactions(){
        return this.transactions;
    }

}
