package modules;

/**
 * Created by xiaolu on 2019-02-15.
 */
public class Transaction {
    private String action;  // [buy | sell]
    private String fundId;
    private int units;

    public Transaction(){}

    public void setFundId(String fundId){
        this.fundId = fundId;
    }

    public String getFundId(){ return this.fundId; }

    public void setAction (String action){
        this.action = action;
    }

    public String getAction (){
        return this.action;
    }

    public void setUnits (int units){
        this.units = units;
    }

    public int getUnits (){
        return this.units;
    }
    
}
