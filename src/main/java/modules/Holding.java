package modules;

/**
 * Created by xiaolu on 2019-02-20.
 */
public class Holding {
    private String fundId;
    private int units;
    private float balance;

    public Holding(){}

    public void setFundId(String fundId){this.fundId = fundId;}

    public String getFundId(){
        return this.fundId;
    }

    public void setUnits(int units){this.units = units;}

    public int getUnits(){
        return this.units;
    }

    public void setbalance(float balance){this.balance = balance;}

    public float getBalance(){
        return this.balance;
    }

}
