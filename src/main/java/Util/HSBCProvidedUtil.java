package Util;

import modules.Holding;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

public class HSBCProvidedUtil {

    private final static String PORTFOLIO_ID = "id";
    private final static String HOLDINGS = "holdings";
    private final static String FUND_ID = "fundId";
    private final static String UNITS = "units";
    private final static String BALANCE= "balance";
    private final static String AMOUNT = "amount";
    private final static String CUSTOMER_ID_KEY = "x-custid";
    private final static String PORTFOLIOS_URL = "https://us-central1-useful-memory-229303.cloudfunctions.net/portfolios2";

    public HSBCProvidedUtil(){
        //
    }

    /**
     * validate if a header contains a customer id
     */
    public static String validateHeader(HttpHeaders headers){
        if(headers.get(CUSTOMER_ID_KEY).size() == 0){
            return null;
        }
        return headers.get(CUSTOMER_ID_KEY).get(0);
    }

    public static HashMap<String, Holding> getHoldings(HttpHeaders headers, String id){
        //retrieve the current customer's holdings
        //send request to HSBC mock-up system. GET /portfolios
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;
        HttpEntity entity = new HttpEntity(headers);
        response = restTemplate.exchange(PORTFOLIOS_URL,
                HttpMethod.GET, entity, String.class);

        //parse response and get customer's holdings
        JSONArray lop = new JSONArray(response.getBody());
        HashMap<String, Holding> holdings = new HashMap<>();
        for (int i = 0; i < lop.length(); i++) {
            JSONObject p = lop.getJSONObject(i);
            if (p.getString(PORTFOLIO_ID).equals(id)) {
                JSONArray lof = p.getJSONArray(HOLDINGS);
                for (int j = 0; j < lof.length(); j++) {
                    JSONObject f = lof.getJSONObject(j);
                    Holding holding = new Holding();
                    holding.setFundId(f.getString(FUND_ID));
                    holding.setUnits(Integer.parseInt(f.getString(UNITS)));
                    holding.setbalance(Float.valueOf(f.getJSONObject(BALANCE).getString(AMOUNT)));
                    holdings.put(holding.getFundId(), holding);
                }
            }
        }
        return holdings;
    }
}
