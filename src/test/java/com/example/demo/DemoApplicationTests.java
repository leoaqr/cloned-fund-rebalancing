package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import modules.Portfolio;
import modules.Recommendation;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Spring is here!");
	}

	@Test
	public void GETPortfolioResponse() {
		String body = this.restTemplate.getForObject("/portfolio/1", String.class);
		assertThat(body).isEqualTo("{\"id\":\"1\",\"deviation\":\"5\",\"type\":\"fund\",\"allocations\":{\"25\":70,\"70\":30}}");
	}

	@Test
	public void POSTportofolioResponse() {
		Map<String, Integer> allocations = new HashMap<>();
		allocations.put("70", 30);
		allocations.put("25", 70);
		Map<String,String> portfolio = new HashMap<>();
		portfolio.put("id", "1");
		portfolio.put("deviation", "5");
		portfolio.put("type", "fund");
		portfolio.put("allocations",allocations.toString());

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);

		Map mapp = new HashMap();
		mapp.put("id", "1");
		mapp.put("deviation", "5");
		mapp.put("type", "fund");
		mapp.put("allocations", new JSONObject(allocations));

		HttpEntity<?> request = new HttpEntity<>(mapp, headers);
		ResponseEntity<Portfolio> response = this.restTemplate.postForEntity("/portfolio/1", request, Portfolio.class);
		assertThat(response.getStatusCode().value()).isEqualTo(200);
	}

	@Test
	public void deviationPUTResponse() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");
		headers.setAll(map);

		Map requestBody = new HashMap();
		requestBody.put("deviation", "5");


		HttpEntity<?> request = new HttpEntity<>(requestBody, headers);
		this.restTemplate.put("/portfolio/1/deviation", request);

		ResponseEntity<Portfolio> response = this.restTemplate.getForEntity("/portfolio/1", Portfolio.class);
		assertThat(response.getStatusCode().value()).isEqualTo(200);
		assertThat(response.getBody().getDeviation()).isEqualTo("5");
	}


    @Test
    // x-custid: nxqa3cu9r6; portfolio_id: 2517972
    public void POSTgetRecommendation_pid2517972() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "nxqa3cu9r6");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/2517972/rebalance", HttpMethod.POST, entity, Recommendation.class);

        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(response.getBody()));
        } catch (Exception e) {
            System.out.println(e);
        }
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    @Test
    // x-custid: nxqa3cu9r6; portfolio_id: 579565575
    public void POSTgetRecommendation_pid579565575() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "nxqa3cu9r6");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/579565575/rebalance", HttpMethod.POST, entity, Recommendation.class);

        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(response.getBody()));
        } catch (Exception e) {
            System.out.println(e);
        }
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    @Test
    // 2 funds in range, 1 fund out of range
    // x-custid: yv9q6aodfa; portfolio_id: 5649768
    public void POSTgetRecommendation_pid5649768() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "yv9q6aodfa");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/5649768/rebalance", HttpMethod.POST, entity, Recommendation.class);

        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(response.getBody()));
        } catch (Exception e) {
            System.out.println(e);
        }
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    @Test
    // x-custid: yv9q6aodfa; portfolio_id: 52654678
    public void POSTgetRecommendation_pid52654678() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "yv9q6aodfa");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/52654678/rebalance", HttpMethod.POST, entity, Recommendation.class);

        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(response.getBody()));
        } catch (Exception e) {
            System.out.println(e);
        }
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }

    @Test
    // within deviation, no recommendation needed.
    // x-custid: t8ej8u8q5n; portfolio_id: 56786761
    public void POSTgetRecommendation_pid56786761() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "t8ej8u8q5n");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/56786761/rebalance", HttpMethod.POST, entity, Recommendation.class);

        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(response.getBody()));
        } catch (Exception e) {
            System.out.println(e);
        }
        assertThat(response.getStatusCode().value()).isEqualTo(200);
    }


    @Test
    // non-existing customerId on mock up system
    public void POSTgetRecommendation_nonexsitingCustomer_on_Mock() {
        HttpHeaders header = new HttpHeaders();
        header.set("x-custid", "acoolname");
        header.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Recommendation> response = restTemplate.exchange("/portfolio/2517972/rebalance", HttpMethod.POST, entity, Recommendation.class);
        assertThat(response.getStatusCode().value()).isEqualTo(404);
    }

}
